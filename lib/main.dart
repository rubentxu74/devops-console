
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'infrastructure/bloc/states.dart';
import 'infrastructure/repositories/authentication_repository.dart';
import 'infrastructure/repositories/user_repository.dart';
import 'infrastructure/ui/pages/dashboard_page.dart';

void main() {
  runApp(AppState(
    authenticationRepository: AuthenticationRepository(),
    userRepository: UserRepository(),
  ));
}

class AppState extends StatelessWidget {
  final AuthenticationRepository authenticationRepository;
  final UserRepository userRepository;

  const AppState(
      {super.key,
      required this.authenticationRepository,
      required this.userRepository});


  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
        value: this.authenticationRepository,
        child: MultiBlocProvider(providers: [
          BlocProvider<PreferencesBloc>(create: (_) => PreferencesBloc()),
          BlocProvider<AuthenticationBloc>(
              create: (_) => AuthenticationBloc(
                    authenticationRepository: authenticationRepository,
                    userRepository: userRepository,
                  ))
        ], child: AppView()));
  }
}

class AppView extends StatelessWidget {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState!;
  DevOpsLocalizationsDelegate _localeDelegate =
      DevOpsLocalizationsDelegate(Locale('es', 'ES'));

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PreferencesBloc, PreferencesState>(builder: _buildWithTheme);
  }


  Widget _buildWithTheme(BuildContext context, PreferencesState state) {

    return MaterialApp(
      navigatorKey: _navigatorKey,
      debugShowCheckedModeBanner: false,
      title: 'DevOps Console',
      supportedLocales: [
        Locale('en'),
        Locale('es'),
      ],
      localizationsDelegates: [
        _localeDelegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      builder: (context, child) {
        return BlocListener<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            print("state status ${state.status}");
            switch (state.status) {
              case AuthenticationStatus.authenticated:
                _navigator.pushAndRemoveUntil<void>(
                    DashboardPage.route(), (route) => false);
                break;
              case AuthenticationStatus.unauthenticated:
                // _navigator.pushAndRemoveUntil<void>(
                //     LoginPage.route(), (route) => false);
                break;
              default:
                break;
            }
          },
          child: child,
        );
      },
      theme: state.themeData,
      routes: {
        // SplashScreen.routeName: (_) => SplashScreen(),
        // LoginPage.routeName: (_) => LoginPage(),
        DashboardPage.routeName: (_) => DashboardPage(),
      },
    );
  }
}
