part of states;


class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({
    required AuthenticationRepository authenticationRepository,
  })   : _authenticationRepository = authenticationRepository,
        super(const LoginState()) {
    on<LoginUsernameChanged>(_mapUsernameChangedToState) ;
    on<LoginPasswordChanged>(_mapPasswordChangedToState) ;
    on<LoginSubmitted>(_mapLoginSubmittedToState) ;
  }

  final AuthenticationRepository _authenticationRepository;

  void _mapUsernameChangedToState(
      LoginUsernameChanged event,
          Emitter<LoginState> emit,
      ) {
    final username = Username.dirty(event.username);
    print("Change username ${event.username}");
    emit(state.copyWith(
      username: username,
      status: Formz.validate([state.password, username]),
    ));
  }

  void _mapPasswordChangedToState(
      LoginPasswordChanged event,
      Emitter<LoginState> emit,
      ) {
    final password = Password.dirty(event.password);
    print("Change password ${event.password}");
    emit(state.copyWith(
      password: password,
      status: Formz.validate([password, state.username]),
    ));
  }

  void _mapLoginSubmittedToState(
      LoginSubmitted event,
      Emitter<LoginState> emit,
      ) async {
    if (state.status.isValidated) {
      print("Isvalidated status");
      emit(state.copyWith(status: FormzStatus.submissionInProgress));
      try {
        await _authenticationRepository.logIn(
          username: state.username.value,
          password: state.password.value,
        );
        emit(state.copyWith(status: FormzStatus.submissionSuccess));
        print("Validated status");
      } on Exception catch (_) {
        print("Error submit");
        emit(state.copyWith(status: FormzStatus.submissionFailure));
      }
    } else {
      print("Not Is validated status");
    }
  }
}