part of states;


@immutable
abstract class PreferencesEvent extends Equatable {
  const PreferencesEvent([List props = const []]): super();
}

class ThemeChanged extends PreferencesEvent {
  final AppTheme theme;

  ThemeChanged({ required this.theme, }) : super([theme]);

  @override
  List<Object?> get props => [theme];
}

class LangChanged extends PreferencesEvent {
  final Locale locale;

  LangChanged({ required this.locale, }) : super([locale]);

  @override
  List<Object?> get props => [locale];
}