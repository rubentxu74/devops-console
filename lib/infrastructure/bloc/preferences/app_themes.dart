part of states;

enum AppTheme {
  Teal,
  BlueGrey,
  Green,
  Brown,
  Blue,
  Red,
}

final Map appThemeData = {
  AppTheme.Teal: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.teal,
    primarySwatch: Colors.teal,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    fontFamily: 'jetbrains',
    useMaterial3: true,
  ),
  AppTheme.BlueGrey: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.blueGrey,
    primarySwatch: Colors.blueGrey,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    fontFamily: 'jetbrains',
    useMaterial3: true,
  ),
  AppTheme.Green: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.green,
    primarySwatch: Colors.green,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    fontFamily: 'jetbrains',
    useMaterial3: true,
  ),
  AppTheme.Brown: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.brown,
    primarySwatch: Colors.brown,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    fontFamily: 'jetbrains',
    useMaterial3: true,
  ),
  AppTheme.Blue: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.blue,
    primarySwatch: Colors.blue,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    fontFamily: 'jetbrains',
    useMaterial3: true,
  ),
  AppTheme.Red: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.red,
    primarySwatch: Colors.red,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    fontFamily: 'jetbrains',
    useMaterial3: true,
  ),
};