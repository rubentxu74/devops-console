part of states;

class PreferencesBloc extends Bloc<PreferencesEvent, PreferencesState> {
  PreferencesBloc()
      : super(PreferencesState(
            locale: Locale('en', 'US'),
            themeData: appThemeData[AppTheme.BlueGrey])) {
    on<ThemeChanged>(fetchThemeEvent);
    on<LangChanged>(onLangChanged);
  }

  FutureOr<void> fetchThemeEvent(ThemeChanged event, Emitter<PreferencesState> emit) {
    emit(state.copyWith(NewThemeData: appThemeData[event.theme]));
  }

  FutureOr<void>  onLangChanged(LangChanged event, Emitter<PreferencesState> emit) {
    DevOpsLocalizations.load(event.locale);
    print("Lang changed ${event.locale}");
    emit(state.copyWith(newLocale: event.locale));
  }
}
