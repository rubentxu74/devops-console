part of states;



class DevOpsLocalizations {
  YamlMap translations;
  String localeName;
  static late DevOpsLocalizations instance;

  DevOpsLocalizations(this.translations, this.localeName);

  static Future<DevOpsLocalizations> load(Locale locale) async {
    final String name =
        locale.countryCode != null ? locale.languageCode : locale.toString();

    final String localeName = Intl.canonicalizedLocale(name);
    String translationsPath = await rootBundle.loadString('i10n/${name}.yaml');
    Intl.defaultLocale = localeName;
    instance = DevOpsLocalizations(loadYaml(translationsPath), localeName);
    return instance;
  }

  static DevOpsLocalizations? of(BuildContext context) {
    return instance;
  }

  String t(String key) {
    var keys = key.split(".");
    dynamic translated = translations;
    keys.forEach((k) => translated = translated[k]);
    return translated;
  }
}

class DevOpsLocalizationsDelegate
    extends LocalizationsDelegate<DevOpsLocalizations> {
  final Locale overriddenLocale;

  const DevOpsLocalizationsDelegate(this.overriddenLocale);

  @override
  bool isSupported(Locale locale) {
    return ['es', 'en'].contains(locale.languageCode);
  }

  @override
  Future<DevOpsLocalizations> load(Locale locale) async {
    print(
        "Is supported locale ${locale.languageCode} OverriddenLocale ${overriddenLocale.languageCode}");
    return DevOpsLocalizations.load(locale);
  }

  @override
  bool shouldReload(covariant LocalizationsDelegate<DevOpsLocalizations> old) {
    return false;
  }
}
