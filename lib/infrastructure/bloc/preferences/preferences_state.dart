part of states;

@immutable
class PreferencesState extends Equatable {
  final Locale locale;
  final ThemeData themeData;

  const PreferencesState({required this.locale, required this.themeData}):super();

  @override
  List<Object> get props => [locale, themeData];

  PreferencesState copyWith({Locale? newLocale, ThemeData? NewThemeData}) {
    return PreferencesState(
        locale: newLocale?? this.locale,
        themeData: NewThemeData?? this.themeData
    );
  }

  @override
  String toString() {
    return 'PreferencesState{locale: ${locale.languageCode}, themeData: ${themeData.colorScheme.primary.toString()} }';
  }
}
