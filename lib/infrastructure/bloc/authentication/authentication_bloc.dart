part of states;

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthenticationRepository _authenticationRepository;
  final UserRepository _userRepository;
  late StreamSubscription<AuthenticationStatus>
  _authenticationStatusSubscription;

  AuthenticationBloc({
    required AuthenticationRepository authenticationRepository,
    required UserRepository userRepository,
  })   : _authenticationRepository = authenticationRepository,
        _userRepository = userRepository,
        super(const AuthenticationState.unknown()) {

    on<AuthenticationStatusChanged>(_mapAuthenticationStatusChangedToState);
    on<AuthenticationLogoutRequested>((event, emit) {
      _authenticationRepository.logOut();
    });
    _authenticationStatusSubscription = _authenticationRepository.status.listen(
          (status) => add(AuthenticationStatusChanged(status)),
    );
  }

  @override
  Future<void> close() {
    _authenticationStatusSubscription.cancel();
    _authenticationRepository.dispose();
    return super.close();
  }

  void _mapAuthenticationStatusChangedToState(
      AuthenticationStatusChanged event,
      Emitter<AuthenticationState> emit,
      ) async {
    print("Auth status ${event.status}");
    switch (event.status) {
      case AuthenticationStatus.unauthenticated:
        emit(AuthenticationState.unauthenticated());
        break;
      case AuthenticationStatus.authenticated:
        final user = await _tryGetUser();
        print("Try User ${user}");
        emit( user != null
            ? AuthenticationState.authenticated(user)
            : const AuthenticationState.unauthenticated());
        break;
      default:
        emit(AuthenticationState.unknown());
    }
  }

  Future<User?> _tryGetUser() async {
    try {
      final user = await _userRepository.getUser();
      print("Try User ${user}");
      return user;
    } on Exception {
      return null;
    }
  }
}