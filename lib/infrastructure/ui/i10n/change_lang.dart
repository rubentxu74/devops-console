import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/preferences/preferences_bloc.dart';

class ChangeLang extends StatelessWidget {

  const ChangeLang({Key? key}) : super(key: key);

  Future<void> onSelected(BuildContext context, int item) async {
    switch (item) {
      case 0:
        BlocProvider.of<PreferencesBloc>(context).add(LangChanged(locale: Locale('es', 'ES')));
        break;
      case 1:
        BlocProvider.of<PreferencesBloc>(context).add(LangChanged(locale: Locale('en', 'US')));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: PopupMenuButton<int>(
            icon: Icon(
              Icons.language,
              color: Theme.of(context).colorScheme.primary,
              size: 36.0,
            ),
            onSelected: (item) => onSelected(context, item),
            itemBuilder: (context) => [
              PopupMenuItem<int>(
                value: 0,
                child: Text('Spanish'),
              ),
              PopupMenuItem<int>(
                value: 1,
                child: Text('English'),
              ),
    ],
      ),
    );
  }
}

