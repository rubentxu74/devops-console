import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/states.dart';


class ChangeTheme extends StatelessWidget {

  const ChangeTheme({Key? key}) : super(key: key);

  void onSelected(BuildContext context, AppTheme item) {
    BlocProvider.of<PreferencesBloc>(context).add(ThemeChanged(theme: item));
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: PopupMenuButton<AppTheme>(
            icon: Icon(
              Icons.colorize,
              color: Theme.of(context).colorScheme.primary,
              size: 36.0,
            ),
            onSelected: (AppTheme item) => onSelected(context, item),
            itemBuilder: (context) => builderMenu(),
      ),
    );
  }
}

List<PopupMenuItem<AppTheme>> builderMenu() {
   List<PopupMenuItem<AppTheme>> items = [];
   for (var itemAppTheme in AppTheme.values) {
      items.add(
        PopupMenuItem(
          value: itemAppTheme,
            child: Text(itemAppTheme.toString())
        )
      );
    }
   return items;
}

