import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';

import '../../../bloc/states.dart';
import '../../utils/utils.dart';

class LoginForm extends StatelessWidget {
  late Responsive responsive;
  late DevOpsLocalizations localizations;

  @override
  Widget build(BuildContext context) {
    responsive = Responsive.of(context);
    localizations = DevOpsLocalizations.of(context)!;
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.status.isSubmissionFailure) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(SnackBar(
              content: Text('Authentication Failed!'),
            ));
        }
      },
      child: Container(
        width: 330,
        height: responsive.scaleHeight(500),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            _CustomInput(
              key: Key('loginForm_usernameInput_textField'),
              iconPath: "assets/images/svg/email-icon.svg",
              placeholder: localizations.t('login.form.userInput.placeholder'),
              onChanged: (user) =>
                  context.read<LoginBloc>().add(LoginUsernameChanged(user)),
              errorMsg: this.localizations.t('login.form.userInput.errorMsg'),
            ),
            Padding(padding: EdgeInsets.all(12)),
            _CustomInput(
              key: Key('loginForm_passwordInput_textField'),
              iconPath: "assets/images/svg/password-icon.svg",
              placeholder:
                  this.localizations.t('login.form.passwordInput.placeholder'),
              isSecret: true,
              onChanged: (password) =>
                  context.read<LoginBloc>().add(LoginPasswordChanged(password)),
              errorMsg:
                  this.localizations.t('login.form.passwordInput.errorMsg'),
            ),
            Padding(padding: EdgeInsets.all(12)),
            new InkWell(
                child: Text(
                        this.localizations.t('login.form.forgotPassword'),
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.primary,
                        ),
                      ),
                onTap: () => {print("click forgotPass")}
            ),
            Padding(padding: EdgeInsets.all(12)),
            _LoginButton(),
            SizedBox(
              height: responsive.scaleHeight(50),
            ),
          ],
        ),
      ),
    );
  }
}

class _CustomInput extends StatelessWidget {
  String iconPath;
  String placeholder;
  Key key;
  String errorMsg;
  bool isSecret;
  Function(String value) onChanged;

  late Responsive responsive;

  //
  _CustomInput(
      {required this.key,
      required this.iconPath,
      required this.placeholder,
      required this.errorMsg,
        required this.onChanged,
      this.isSecret = false});

  @override
  Widget build(BuildContext context) {
    responsive = Responsive.of(context);
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (context, state) {
        return TextField(
          key: this.key,
          onChanged: this.onChanged,
          obscureText: this.isSecret,
          decoration: InputDecoration(
            labelText: this.placeholder,
            icon: SvgPicture.asset(
              this.iconPath,
              color: Color(0xffcccccc),
              width: responsive?.scaleWidth(45),
            ),
            errorText: state.password.invalid ? this.errorMsg : null,
          ),
        );
      },
    );
  }
}

class _LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return state.status.isSubmissionInProgress
            ? Center(child: const CircularProgressIndicator())
            : ElevatedButton(
                key: const Key('loginForm_continue_raisedButton'),
                child: const Text('Login'),
                onPressed:
                     () {
                        print("Login submitted");
                        context.read<LoginBloc>().add(const LoginSubmitted());
                      }
                    ,
                style: ElevatedButton.styleFrom(
                  foregroundColor: Theme.of(context).selectedRowColor,
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  minimumSize: Size.fromHeight(50),
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4)),
                  ),
                ),
              );
      },
    );
  }
}
