import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class InputTextLogin extends StatelessWidget {
  final String iconPath, placeholder;
  final bool isPassword;

  const InputTextLogin(
      {Key? key, required this.iconPath, required this.placeholder, required this.isPassword})
      : assert(iconPath != null && placeholder != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoTextField(
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
      prefix: Container(
        width: 40,
        height: 30,
        padding: EdgeInsets.all(2),
        child: SvgPicture.asset(this.iconPath, color: Color(0xffcccccc)),
      ),
      placeholder: this.placeholder,
      obscureText: this.isPassword,
      decoration: BoxDecoration(
          border: Border(
        bottom: BorderSide(width: 1, color: Colors.grey),
      )),
    );
  }
}
