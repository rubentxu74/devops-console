import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../bloc/states.dart';
import '../../utils/utils.dart';

class Welcome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Welcome();
}

class _Welcome extends State<Welcome> {
  late Responsive responsive;
  late DevOpsLocalizations localizations;

  @override
  Widget build(BuildContext context) {
    responsive = Responsive.of(context);
    localizations = DevOpsLocalizations.of(context)!;

    double defaultHeight = responsive.height;
    if(responsive.width <= ScreenSize.Large.width)  {
      defaultHeight = 450.0;
    }
    return Container(
      height:responsive?.scaleHeight(defaultHeight),
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Colors.white,
                 Theme.of(context).colorScheme.primary,
              ]
          )
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            // widthFactor: 2,
              child: SvgPicture.asset("assets/images/svg/person-devops.svg",
                width: responsive?.scaleWidth(280),)),

          SizedBox(
            height: responsive?.scaleHeight(20),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: responsive?.scaleWidth(10)),
            child: Center(
              child: Column(
                children: [
                  Text("DevOps Console",
                    style: TextStyle(
                        fontSize: responsive?.scaleWidth(30),
                        overflow: TextOverflow.fade,
                        // decoration: TextDecoration.none,
                        fontWeight: FontWeight.bold,
                        // fontFamily: 'jetbrains',
                        color: Colors.white,
                        shadows: [
                          Shadow(
                              color: Colors.black.withOpacity(0.7),
                              offset: Offset(2, 5),
                              blurRadius: 4
                          )
                        ]
                    ),
                  ),
                  SizedBox(
                    height: responsive?.scaleHeight(10),
                  ),
                  Text(
                    this.localizations.t('login.welcomeSubTitle'),
                    style: TextStyle(
                        fontSize: responsive?.scaleWidth(12),
                        fontWeight: FontWeight.w200,
                        color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ],

              ),
            ),
          ),
          SizedBox(
            height: responsive?.scaleHeight(20),
          ),
        ],

      ),
    );
  }
}
