import 'package:flutter/material.dart';
import '../../../shared/selection_button.dart';

class MainMenu extends StatelessWidget {
  const MainMenu({
    required this.onSelected,
    Key? key,
  }) : super(key: key);

  final Function(int index, SelectionButtonData value) onSelected;

  @override
  Widget build(BuildContext context) {
    return SelectionButton(
      data: [
        SelectionButtonData(
          activeIcon: Icons.home,
          icon: Icons.home_outlined,
          label: "Home",
        ),
        SelectionButtonData(
          activeIcon: Icons.notification_add,
          icon: Icons.no_accounts_outlined,
          label: "Notifications",
          totalNotif: 100,
        ),
        SelectionButtonData(
          activeIcon: Icons.check,
          icon: Icons.check_box_outlined,
          label: "Task",
          totalNotif: 20,
        ),
        SelectionButtonData(
          activeIcon: Icons.settings,
          icon: Icons.settings_outlined,
          label: "Settings",
        ),
      ],
      onSelected: onSelected,
    );
  }
}