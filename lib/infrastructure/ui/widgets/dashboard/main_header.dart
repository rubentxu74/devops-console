
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../shared/colors.dart';
import '../../shared/spacing.dart';
import '../../shared/text_styles.dart';
import '../../utils/utils.dart';




class MainHeader extends StatelessWidget {
  late Responsive responsive;

  MainHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    responsive = Responsive.of(context);
    return Row(
      children: [
        if (!responsive.isLargeSize())
          IconButton(
            icon: const Icon(
              Icons.menu,
              color: kBlackColor,
            ),
            onPressed: () {},
          ),
        horizontalSpaceSmall,
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              'Welcome back,',
              style: kBodyRegularTextStyle,
            ),
            Text(
              'Ayomide Ajibade',
              style: kHeading3TextStyle,
            ),
          ],
        ),
        const Spacer(),
        const Icon(
          Icons.mark_email_unread,
          color: kSecondaryColor4,
        ),
        horizontalSpaceSmall,
        const Icon(
          Icons.notifications_outlined,
          color: kSecondaryColor4,
        ),
        horizontalSpaceSmall,
        const CircleAvatar(
          radius: 15,
          backgroundImage: AssetImage('assets/images/main/images/image_1.png'),
        ),
      ],
    );
  }
}
