
import 'package:devops_console/external/ui/widgets/transactions_card.dart';

import 'package:flutter/material.dart';

import '../../../../external/models/transaction/transaction_model.dart';
import '../../../../external/ui/widgets/transactions_row.dart';
import '../../shared/colors.dart';
import '../../shared/edge_insect.dart';
import '../../shared/spacing.dart';
import '../../shared/text_styles.dart';
import '../../utils/utils.dart';
import 'task_gridview.dart';


class DashBoardView extends StatelessWidget {
  late Responsive responsive;


  DashBoardView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    responsive = Responsive.of(context);

    return ChildSizeNotifier(
      child: Text('small'),
      builder: (context, size, child) {
        // size is the size of the text
        return body(size,context);
      },
    );
  }

  Widget body(Size size,BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Column(
            children: [
              verticalSpaceSmall,
              // MainHeader(),
              verticalSpaceRegular,
              Container(
                padding: kEdgeInsetsHorizontalNormal,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Theme.of(context).colorScheme.primary,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Wallet balance',
                          style: kBodyRegularTextStyle.copyWith(
                            color: kWhiteColor,
                          ),
                        ),
                        verticalSpaceSmall,
                        Text(
                          '\$ 56,439.00',
                          style: kHeading1TextStyle.copyWith(
                            color: kWhiteColor,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      padding: kEdgeInsetsHorizontalNormal,
                      height: responsive.scaleHeight(20),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: kWhiteColor),
                      child: Center(
                        child: Text(
                          'Fund wallet',
                          style: kBodyTextStyle.copyWith(color: kBlackColor),
                        ),
                      ),
                    ),
                  ],
                ),
                height: responsive.scaleHeight(120),
              ),
              verticalSpaceRegular,
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      transactionsGridView(size),
                      verticalSpaceRegular,
                      const TransactionRow(
                        title: 'Transactions',
                        cardText: 'See all',
                      ),
                      verticalSpaceRegular,
                      for (final transaction in transactionsData) ...[
                        TransactionsCard(transaction: transaction),
                      ],
                      verticalSpaceRegular,
                      if (!responsive.isLargeSize())
                        const SizedBox(height: defaultPadding),
                      verticalSpaceRegular
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget transactionsGridView(Size dashboardSize) {
    int _crossAxisCount = getCrossAxisCount(dashboardSize);
    double _aspectRatio = getAspectRatio(dashboardSize);
    print(' _crossAxisCount -> ${_crossAxisCount} _aspectRatio -> ${_aspectRatio}');
    print('Dashboar size -> ${dashboardSize}');
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        // color: Colors.teal,
      ),
      child: TaskGridView(
          crossAxisCount:  _crossAxisCount,
          childAspectRatio: _aspectRatio,
          dashboardSize: dashboardSize,
      ),
    );

  }

  double getAspectRatio(Size dashboardSize) {
    if (dashboardSize.width > ScreenSize.ExtraLarge.width) return 1.2;
    if (dashboardSize.width >
        ScreenSize.Large.width + (ScreenSize.ExtraLarge.width - ScreenSize.Large.width )/ 2) return 1.3;
    if (dashboardSize.width > ScreenSize.Large.width) return 1.5;
    if (dashboardSize.width >
        ScreenSize.Normal.width + (ScreenSize.Large.width - ScreenSize.Normal.width )/ 2) return 1.7;
    if (dashboardSize.width > ScreenSize.Normal.width) return 1.6;
    return 2.4;
  }

  int getCrossAxisCount(Size dashboardSize) {
    if (dashboardSize.width > ScreenSize.ExtraLarge.width) return 5;
    if (dashboardSize.width > ScreenSize.Large.width + (ScreenSize.ExtraLarge.width - ScreenSize.Large.width )/ 2) return 4;
    if (dashboardSize.width > ScreenSize.Large.width) return 3;
    if (dashboardSize.width > ScreenSize.Normal.width + (ScreenSize.Large.width - ScreenSize.Normal.width )/ 2) return 2;
    if (dashboardSize.width > ScreenSize.Normal.width ) return 2;
    return 1;

  }
}
