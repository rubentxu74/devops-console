
import 'package:devops_console/infrastructure/ui/shared/selection_button.dart';
import 'package:flutter/material.dart';

import '../../shared/colors.dart';
import '../../shared/text_styles.dart';
import 'new/main_menu.dart';
import 'new/member.dart';
import 'new/task_menu.dart';
import 'new/user_profile.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: kTertiaryColor5,
      child: SafeArea(
        child: SingleChildScrollView(child: _buildSidebar(context)),
      ),
    );
  }
}
Widget _buildSidebar(BuildContext context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: UserProfile(
          data: UserProfileData(
            image: AssetImage('assets/images/main/icons/face1.png'),
            name: "Firgia",
            jobDesk: "Project Manager",
          ),
          onPressed: () {},
        ),
      ),
      const SizedBox(height: 15),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: MainMenu(onSelected: (int index, SelectionButtonData value) =>{}),
      ),
      const Divider(
        indent: 20,
        thickness: 1,
        endIndent: 20,
        height: 60,
      ),
      Member(member: []),
      const SizedBox(height: 4),
      TaskMenu(
        onSelected: (i,s){},
      ),
      const SizedBox(height: 10),
      Padding(
        padding: const EdgeInsets.all(5),
        child: Text(
          "2021 Teamwork lisence",
          style: Theme.of(context).textTheme.caption,
        ),
      ),
    ],
  );
}
