import 'package:devops_console/infrastructure/ui/widgets/dashboard/task_card.dart';
import 'package:flutter/material.dart';

import '../../shared/edge_insect.dart';
import 'forms/form_stepper.dart';

class TaskGridView extends StatelessWidget {
  final Size dashboardSize;
  final int crossAxisCount;
  final double childAspectRatio;

  const TaskGridView({
    Key? key,
    this.crossAxisCount = 4,
    this.childAspectRatio = 1,
    required Size this.dashboardSize,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: 8,

      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: crossAxisCount,
        crossAxisSpacing: defaultPadding,
        mainAxisSpacing: defaultPadding,
        childAspectRatio: childAspectRatio,
      ),
      itemBuilder: (context, index) => itemTaskCarBuilder(context,index,dashboardSize),
    );
  }
}

Widget itemTaskCarBuilder(context, index, Size dashboardSize) {
  return GestureDetector(
    child: Container(

      constraints: BoxConstraints(
          minHeight: 300, minWidth: double.infinity, maxHeight: 300),
      child: Hero(
        tag: 'TaskCardHero$index',
        child: CardTask(
          primary: Theme.of(context).backgroundColor,
          onPrimary: Colors.white,
          data : CardTaskData(
            label: "Determine meeting schedule $index",
            jobDesk: "System Analyst $index",
            dueDate: DateTime.now().add(const Duration(minutes: 50)),
          ),

        ),
      ),
    ),
    onTap: () {
      print("On tap card");
      Navigator.of(context).push(
        PageRouteBuilder(
          opaque: false,
          pageBuilder: (_, __, ___) => ExpandedTaskCard(id: 'TaskCardHero$index', dashboardSize: dashboardSize,),
        ),
      );
    },
  );
}

class ExpandedTaskCard extends StatelessWidget {
  final id;
  final Size dashboardSize;

  const ExpandedTaskCard({required this.id, required this.dashboardSize});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: GestureDetector(
        child: Align(
          alignment: Alignment.topRight,
          child: Hero(
            tag: this.id,
            child: Container(
              width: this.dashboardSize.width + 18 ?? double.infinity,
              height: this.dashboardSize.height + 18 ?? double.infinity,
              child: Card(
                elevation: 4.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(width: 2, color: Theme.of(context).primaryColor)),
                child: StepperForm(),

              ),
            ),
          ),
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
