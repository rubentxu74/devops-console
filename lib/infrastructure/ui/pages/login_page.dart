import 'package:devops_console/infrastructure/bloc/states.dart';
import 'package:devops_console/infrastructure/ui/theme/change_theme.dart';
import 'package:devops_console/infrastructure/ui/utils/utils.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../repositories/authentication_repository.dart';
import '../i10n/change_lang.dart';

import '../widgets/login/login_form.dart';
import '../widgets/login/welcome.dart';

class LoginPage extends StatelessWidget {
  static const routeName = 'login';
  late Responsive responsive;
  late DevOpsLocalizations localizations;

  LoginPage({super.key});

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => LoginPage());
  }

  @override
  void InitState() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));
  }

  @override
  Widget build(BuildContext context) {
    responsive = Responsive.of(context);
    print("Screen size is : ${responsive.getSize()} size pixel is: ${responsive.width}x${responsive.height} ");
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: BlocProvider(
          create: (context) => LoginBloc(
            authenticationRepository:
                RepositoryProvider.of<AuthenticationRepository>(context),
          ),
          child: (responsive.isLargeSize() || responsive.isExtraLargeSize())
              ? LargeScreenView(responsive: responsive)
              : NormalScreenView(responsive: responsive),
        ),
      ),
    );
  }
}

class NormalScreenView extends StatelessWidget {
  const NormalScreenView({
    Key? key,
    required this.responsive,
  }) : super(key: key);

  final Responsive responsive;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Welcome(),
          SizedBox(
            height: responsive.scaleHeight(10),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ChangeTheme(),
              ChangeLang(),
            ],
          ),
          LoginForm(),
        ],
      ),
    );
  }
}

class LargeScreenView extends StatelessWidget {
  const LargeScreenView({
    Key? key,
    required this.responsive,
  }) : super(key: key);

  final Responsive responsive;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Container(
                  height: responsive.height, child: Center(child: Welcome()))),
        ),
        Expanded(
          child: Container(
              height: responsive.height,
              child: Center(
                  child: SingleChildScrollView(
                child: Column(children: [
                  SizedBox(
                    height: responsive.scaleHeight(50),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ChangeTheme(),
                      ChangeLang(),
                    ],
                  ),
                  LoginForm()
                ]),
              ))),
        )
      ],
    );
  }
}
