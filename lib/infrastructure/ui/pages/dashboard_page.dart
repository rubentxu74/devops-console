library dashboard;
import 'package:flutter/material.dart';

import '../shared/spacing.dart';

import '../utils/utils.dart';
import '../widgets/dashboard/dashboard_view.dart';
import '../widgets/dashboard/side_menu.dart';


class DashboardPage extends StatelessWidget {
  // static const routeName = '/dashboard';
  static const routeName = '/';
  late Responsive responsive;

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => DashboardPage());
  }

  DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    responsive = Responsive.of(context);
    print("Screen size is : ${responsive.getSize()} size pixel is: ${responsive.width}x${responsive.height} ");
    return Scaffold(
      drawer: const SideMenu(),
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (responsive.isLargeSize() || responsive.isExtraLargeSize())
            const Expanded(
              flex: 2,
              child: SideMenu(),
            ),
          horizontalSpaceRegular,
          Expanded(
            flex: 5,
            child: DashBoardView(),
          ),
          horizontalSpaceSmall,
        ],
      ),
    );
  }
}
