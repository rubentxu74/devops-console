part of utils;

enum ScreenSize {
  Small(300.0, 0.7),
  Normal(600.0, 1.0),
  Large(900.0, 1.1),
  ExtraLarge(1200.0, 1.3);

  const ScreenSize(this.width,this.scaleFactor);
  final double width;
  final double scaleFactor;
}

class Responsive {
  BuildContext _context;
  late MediaQueryData _mediaQueryData;
  double? _textScaleFactor;

  Responsive(this._context) {
    this._mediaQueryData = MediaQuery.of(_context);
    _textScaleFactor = _mediaQueryData.textScaleFactor;
  }

  bool isSmallSize() =>
      getSize() == ScreenSize.Small;

  bool isNormalSize() =>
      getSize() == ScreenSize.Normal;

  bool isLargeSize() =>
      getSize() == ScreenSize.Large;

  bool isExtraLargeSize() =>
      getSize() == ScreenSize.ExtraLarge;

  static Responsive of(BuildContext context) => Responsive(context);
  
  double get scaleFactor {
    return getSize().scaleFactor;
  }

  ScreenSize getSize() {
    double deviceWidth = MediaQuery.of(_context).size.width;
    if (deviceWidth > ScreenSize.ExtraLarge.width) return ScreenSize.ExtraLarge;
    if (deviceWidth > ScreenSize.Large.width) return ScreenSize.Large;
    if (deviceWidth > ScreenSize.Normal.width) return ScreenSize.Normal;
    if (deviceWidth > ScreenSize.Small.width) return ScreenSize.Small;
    return ScreenSize.Small;
  }

  get _scaleWidth => (width * scaleFactor) / width;

  get _scaleHeight => (height * scaleFactor) / height;

  get width => _mediaQueryData.size.width;

  get height => _mediaQueryData.size.height;

  scaleWidth(double width) => width * _scaleWidth;

  scaleHeight(double height) => height * _scaleHeight;

  scaleSp(double fontSize) => scaleWidth(fontSize) * _textScaleFactor;



//Spaces
  get barSpace1Width => scaleWidth(60);

  get barSpace2Width => scaleWidth(80);

//Text Size
  get bodyText1 => scaleSp(12);

  get headline6 => scaleSp(15);

  get headline3 => scaleSp(30);

  get headline2 => scaleSp(40);

//Spacing
  get letterSpacingCarouselWidth => scaleWidth(10);

  get letterSpacingHeaderWidth => scaleWidth(3);

  static WidgetInfo getWidgetInfo(GlobalKey widgetKey ) {
    final RenderBox renderBox = widgetKey.currentContext?.findRenderObject() as RenderBox;

    final Size size = renderBox.size; // or _widgetKey.currentContext?.size


    final Offset offset = renderBox.localToGlobal(Offset.zero);
    print('Offset: ${offset.dx}, ${offset.dy}');
    print('Position: ${(offset.dx + size.width) / 2}, ${(offset.dy + size.height) / 2}');
    double positionX = (offset.dx + size.width) / 2;
    double positionY = (offset.dy + size.height) / 2;
    return WidgetInfo(
        size: size,
        offset: offset,
        positionX: positionX,
        positionY: positionY
    );
  }
}

@immutable
class WidgetInfo {
  Size size;
  Offset offset;
  double positionX;
  double positionY;

  WidgetInfo({required this.size, required this.offset, required this.positionX, required this.positionY});

  @override
  String toString() {
    return 'WidgetInfo{width: ${size.width}, height: ${size.height} offset: $offset, positionX: $positionX, positionY: $positionY}';
  }
}