part of utils;

class Insets {
  static const double _xsmall = 3;
  static const double _small = 5;
  static const double _medium = 10;
  static const double _large = 20;
  static const double _extraLarge = 100;

  Responsive _responsive;
  
  Insets(this._responsive) {}
  
  get xsmall => _responsive.scaleWidth(_xsmall);

  get small => this._responsive.scaleWidth(_small);

  get medium => this._responsive.scaleWidth(_medium);

  get large => this._responsive.scaleWidth(_large);

  get extraLarge => this._responsive.scaleWidth(_extraLarge);
// etc
}