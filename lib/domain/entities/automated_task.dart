class AutomatedTask {
  final String title;
  final String description;
  final String time;
  final String date;
  final String status;

  AutomatedTask({
    required this.title,
    required this.description,
    required this.time,
    required this.date,
    required this.status,
  });
}